/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ChattypeareaComponent } from './chattypearea.component';

describe('ChattypeareaComponent', () => {
  let component: ChattypeareaComponent;
  let fixture: ComponentFixture<ChattypeareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChattypeareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChattypeareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
