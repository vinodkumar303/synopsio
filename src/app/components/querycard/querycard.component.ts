import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-querycard',
  templateUrl: './querycard.component.html',
  styleUrls: ['./querycard.component.scss']
})
export class QuerycardComponent implements OnInit {
  showHeaderText: boolean;
  buttonName = 'expand';
  detailedInformation = false;
  public detailstabs = false;
  public showList = false;
  public findResult = true;
  public searchResult = false;
  constructor() {
    this.showHeaderText = true;
  }

  ngOnInit() {
  }

  showDetailsTabs() {
    this.detailstabs = !this.detailstabs;
  }

  toggle() {
    this.detailedInformation = !this.detailedInformation;
    if (this.detailedInformation) {
      this.buttonName = 'collapse';
    } else {
      this.buttonName = 'expand';
    }
  }
  closeDetailsView() {
    this.detailstabs = false;
  }

  showListView() {
    this.showList = !this.showList;
    this.findResult = !this.findResult;
    this.searchResult = !this.searchResult;
  }

  tabselect(event) {
    if (event === 2) {
      this.showHeaderText = false;
    } else {
      this.showHeaderText = true;
    }
  }
}
