import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { TabsModule } from 'ngx-tabs';
import { ChatsComponent } from './components/chats/chats.component';
import { QuerycardComponent } from './components/querycard/querycard.component';
import { ChattypeareaComponent } from './components/chattypearea/chattypearea.component';
import { ShareconversationComponent } from './components/shareconversation/shareconversation.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatsComponent,
    QuerycardComponent,
    ChattypeareaComponent,
    ShareconversationComponent
  ],
  imports: [
    BrowserModule,
    AngularFontAwesomeModule,
    TabsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
